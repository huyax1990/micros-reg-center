package com.xxtv.micros.rc;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Created by yaxiong.hu on 2016/7/13.
 */

@EnableEurekaServer
@SpringBootApplication
public class RCenterApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(RCenterApplication.class).web(true).run(args);
    }
}
